<div class="header">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-12 col-12"> <a href="{{url('/')}}" class="logo"><img src="{{ asset('/') }}sitesetting_images/thumb/{{ $siteSetting->site_logo }}" alt="{{ $siteSetting->site_name }}" /></a>
                    <div class="navbar-header navbar-light">
                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#nav-main" aria-controls="nav-main" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-10 col-md-12 col-12"> 

                    <!-- Nav start -->
                    <nav class="navbar navbar-expand-lg navbar-light">
    					
                        <div class="navbar-collapse collapse" id="nav-main">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item {{ Request::url() == route('index') ? 'active' : '' }}"><a href="{{url('/')}}" class="nav-link">{{__('Home')}}</a> </li>
    							
                                
    							@if(Auth::guard('company')->check())
    							<li class="nav-item"><a href="{{url('/job-seekers')}}" class="nav-link">{{__('Seekers')}}</a> </li>
    							@else
    							<li class="nav-item"><a href="{{url('/jobs')}}" class="nav-link">{{__('Jobs')}}</a> </li>
    							@endif

    							<li class="nav-item {{ Request::url()}}"><a href="{{url('/companies')}}" class="nav-link">{{__('Companies')}}</a> </li>
                                @foreach($show_in_top_menu as $top_menu) @php $cmsContent = App\CmsContent::getContentBySlug($top_menu->page_slug); @endphp
                                <li class="nav-item {{ Request::url() == route('cms', $top_menu->page_slug) ? 'active' : '' }}"><a href="{{ route('cms', $top_menu->page_slug) }}" class="nav-link">{{ $cmsContent->page_title }}</a> </li>
                                @endforeach
    							<li class="nav-item {{ Request::url() == route('blogs') ? 'active' : '' }}"><a href="{{ route('blogs') }}" class="nav-link">{{__('Blog')}}</a> </li>
                                <li class="nav-item {{ Request::url() == route('contact.us') ? 'active' : '' }}"><a href="{{ route('contact.us') }}" class="nav-link">{{__('Contact us')}}</a> </li>
                                @if(Auth::check())
                                <li class="nav-item dropdown userbtn"><a href="">{{Auth::user()->printUserImage()}}</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item"><a href="{{route('home')}}" class="nav-link"><i class="fa fa-tachometer" aria-hidden="true"></i> {{__('Dashboard')}}</a> </li>
                                        <li class="nav-item"><a href="{{ route('my.profile') }}" class="nav-link"><i class="fa fa-user" aria-hidden="true"></i> {{__('My Profile')}}</a> </li>
                                        <li class="nav-item"><a href="{{ route('view.public.profile', Auth::user()->id) }}" class="nav-link"><i class="fa fa-eye" aria-hidden="true"></i> {{__('View Public Profile')}}</a> </li>
                                        <li><a href="{{ route('my.job.applications') }}" class="nav-link"><i class="fa fa-desktop" aria-hidden="true"></i> {{__('My Job Applications')}}</a> </li>
                                        <li class="nav-item"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header').submit();" class="nav-link"><i class="fa fa-sign-out" aria-hidden="true"></i> {{__('Logout')}}</a> </li>
                                        <form id="logout-form-header" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </ul>
                                </li>
                                @endif @if(Auth::guard('company')->check())
                                <li class="nav-item postjob"><a href="{{route('post.job')}}" class="nav-link register">{{__('Post a job')}}</a> </li>
                                <li class="nav-item dropdown userbtn"><a href="">{{Auth::guard('company')->user()->printCompanyImage()}}</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item"><a href="{{route('company.home')}}" class="nav-link"><i class="fa fa-tachometer" aria-hidden="true"></i> {{__('Dashboard')}}</a> </li>
                                        <li class="nav-item"><a href="{{ route('company.profile') }}" class="nav-link"><i class="fa fa-user" aria-hidden="true"></i> {{__('Company Profile')}}</a></li>
                                        <li class="nav-item"><a href="{{ route('post.job') }}" class="nav-link"><i class="fa fa-desktop" aria-hidden="true"></i> {{__('Post Job')}}</a></li>
                                        <li class="nav-item"><a href="{{route('company.messages')}}" class="nav-link"><i class="fa fa-envelope-o" aria-hidden="true"></i> {{__('Company Messages')}}</a></li>
                                        <li class="nav-item"><a href="{{ route('company.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header1').submit();" class="nav-link"><i class="fa fa-sign-out" aria-hidden="true"></i> {{__('Logout')}}</a> </li>
                                        <form id="logout-form-header1" action="{{ route('company.logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </ul>
                                </li>
                                @endif @if(!Auth::user() && !Auth::guard('company')->user())
                                <li class="nav-item"><a href="{{route('login')}}" class="nav-link">{{__('Sign in')}}</a> </li>
    							<li class="nav-item"><a href="{{route('register')}}" class="nav-link register">{{__('Register')}}</a> </li>                            
                                @endif
                                <li class="dropdown userbtn"><a href="{{url('/')}}"><img src="{{asset('/')}}images/lang.png" alt="" class="userimg" /></a>
                                    <ul class="dropdown-menu">
                                        @foreach($siteLanguages as $siteLang)
                                        <li><a href="javascript:;" onclick="event.preventDefault(); document.getElementById('locale-form-{{$siteLang->iso_code}}').submit();" class="nav-link">{{$siteLang->native}}</a>
                                            <form id="locale-form-{{$siteLang->iso_code}}" action="{{ route('set.locale') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="locale" value="{{$siteLang->iso_code}}"/>
                                                <input type="hidden" name="return_url" value="{{url()->full()}}"/>
                                                <input type="hidden" name="is_rtl" value="{{$siteLang->is_rtl}}"/>
                                            </form>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>

                            <!-- Nav collapes end --> 

                        </div>
                        <div class="clearfix"></div>
                    </nav>

                    <!-- Nav end --> 

                </div>
            </div>
        </div>
        <!-- row end --> 

    </div>

    <!-- Header container end --> 

</div>


<!-- second menu -->
<div class="second-menu bg1 cf-white">
    <div class="container-fluid">
        <div class="container submenu">
            <div class="row">

                <div class="col-lg-6 col-md-12 col-12">
                
                    <!-- DÀNH CHO ỨNG VIÊN -->
                        <div class="dropdown dropdown-submenu fl">
                        
                            <a target="_blank" href="#" class="">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.26142 21.75C5.68195 21.75 6.02284 21.409 6.02284 20.9883C6.02284 18.0062 8.44801 15.5801 11.4289 15.5801H12.5711C15.552 15.5801 17.9772 18.0062 17.9772 20.9883C17.9772 21.409 18.318 21.75 18.7386 21.75C19.1591 21.75 19.5 21.409 19.5 20.9883C19.5 17.1662 16.3917 14.0566 12.5711 14.0566H11.4289C7.60831 14.0566 4.5 17.1662 4.5 20.9883C4.5 21.409 4.84089 21.75 5.26142 21.75ZM6.93655 7.3916C6.93655 4.55652 9.24217 2.25 12.0761 2.25C14.9101 2.25 17.2157 4.55652 17.2157 7.3916C17.2157 10.2267 14.9101 12.5332 12.0761 12.5332C9.24217 12.5332 6.93655 10.2267 6.93655 7.3916ZM8.45939 7.3916C8.45939 9.38666 10.0819 11.0098 12.0761 11.0098C14.0704 11.0098 15.6929 9.38666 15.6929 7.3916C15.6929 5.39655 14.0704 3.77344 12.0761 3.77344C10.0819 3.77344 8.45939 5.39655 8.45939 7.3916Z" fill="white"></path><path d="M12.0368 21.75C12.1643 21.75 12.2917 21.7152 12.4058 21.6457L15.3837 19.8314C15.555 19.7271 15.6789 19.5546 15.7275 19.353C15.7761 19.1515 15.7452 18.9379 15.6418 18.7605L14.5389 16.869C14.3268 16.5054 13.8742 16.3911 13.5278 16.6137C13.1815 16.8364 13.0726 17.3116 13.2847 17.6753L13.9945 18.8926L12.0323 20.0881L10.0195 18.8914L10.7464 17.685C10.9635 17.3246 10.8613 16.8477 10.5181 16.6198C10.175 16.3918 9.72073 16.4991 9.50363 16.8594L8.36392 18.7509C8.25631 18.9295 8.22315 19.1465 8.27208 19.3515C8.32105 19.5565 8.44785 19.7314 8.623 19.8355L11.6745 21.6498C11.7869 21.7166 11.9119 21.75 12.0368 21.75Z" fill="white"></path>
                                </svg> 
                            Dành cho cộng tác viên
                        </a>
                    
                            <ul class="dropdown-menu">
                                <li>
                                    <a target="_blank" href="#">Chính sách cộng tác viên
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="#">Hướng dẫn đăng ký
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="#">Kiến thức
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="#">Hỏi đáp
                                    </a>
                                </li>
                            </ul>
                        </div>
                  
                    <!-- end DÀNH CHO ỨNG VIÊN -->

                    <!-- DÀNH CHO NHÀ TUYỂN DỤNG -->
                        <div class="dropdown dropdown-submenu fl">
                        
                            <a target="_blank" href="#" class="">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M4.99267 16.2656C6.50472 16.2656 7.73486 17.4958 7.73486 19.0078C7.73486 20.5199 6.50472 21.75 4.99267 21.75C3.48062 21.75 2.25048 20.5199 2.25048 19.0078C2.25048 17.4958 3.48062 16.2656 4.99267 16.2656ZM4.99267 20.2266C5.6647 20.2266 6.21142 19.6798 6.21142 19.0078C6.21142 18.3358 5.6647 17.7891 4.99267 17.7891C4.32065 17.7891 3.77392 18.3358 3.77392 19.0078C3.77392 19.6798 4.32069 20.2266 4.99267 20.2266Z" fill="white"></path><path d="M19.0078 16.2656C20.5199 16.2656 21.75 17.4958 21.75 19.0078C21.75 20.5199 20.5199 21.75 19.0078 21.75C17.4958 21.75 16.2656 20.5199 16.2656 19.0078C16.2656 17.4958 17.4958 16.2656 19.0078 16.2656ZM19.0078 20.2266C19.6798 20.2266 20.2266 19.6798 20.2266 19.0078C20.2266 18.3358 19.6798 17.7891 19.0078 17.7891C18.3358 17.7891 17.7891 18.3358 17.7891 19.0078C17.7891 19.6798 18.3358 20.2266 19.0078 20.2266Z" fill="white"></path><path d="M7.88875 2.26383C8.30187 2.18449 8.70116 2.45486 8.7806 2.86798L8.86287 3.2958C8.98939 3.83898 9.47312 4.26866 10.0145 4.26866C10.6581 4.26866 11.1622 3.77385 11.1622 3.1422V3.01183C11.1622 2.59114 11.5032 2.25012 11.9239 2.25012C12.3446 2.25012 12.6857 2.59114 12.6857 3.01183C12.6857 3.01183 12.6922 3.10986 12.6922 3.1422C12.6922 3.77385 13.1963 4.26866 13.84 4.26866C14.3814 4.26866 14.837 3.88975 14.9782 3.33133L15.0673 2.86798C15.1467 2.45486 15.5462 2.18446 15.9592 2.26383C16.3723 2.34327 16.6427 2.74256 16.5633 3.15568L15.7365 7.45495C15.5611 8.36726 14.7595 9.02938 13.8305 9.02938H10.0174C9.08838 9.02938 8.28682 8.36726 8.11144 7.45499L7.2846 3.15568C7.20519 2.74256 7.47567 2.34327 7.88875 2.26383ZM9.60745 7.16733C9.64515 7.36351 9.81756 7.50595 10.0174 7.50595H13.8305C14.0303 7.50595 14.2027 7.36355 14.2405 7.16729L14.5214 5.70663C14.3034 5.76254 14.0749 5.79205 13.84 5.79205C13.1274 5.79205 12.43 5.49037 11.9273 4.99541C11.9297 4.99301 11.932 4.99061 11.9272 4.99537C11.9224 4.99061 11.9248 4.99301 11.9272 4.99541C11.4245 5.49034 10.7271 5.79205 10.0146 5.79205C9.77712 5.79205 9.54632 5.76181 9.32618 5.70472L9.60745 7.16733Z" fill="white"></path><path d="M11.8857 16.2656C13.3978 16.2656 14.6279 17.4958 14.6279 19.0078C14.6279 20.5199 13.3978 21.75 11.8857 21.75C10.3737 21.75 9.14355 20.5199 9.14355 19.0078C9.14355 17.4958 10.3737 16.2656 11.8857 16.2656ZM11.8857 20.2266C12.5578 20.2266 13.1045 19.6798 13.1045 19.0078C13.1045 18.3358 12.5578 17.7891 11.8857 17.7891C11.2137 17.7891 10.667 18.3358 10.667 19.0078C10.667 19.6798 11.2138 20.2266 11.8857 20.2266Z" fill="white"></path><path d="M11.9268 4.9954C11.9243 4.9979 11.9219 5.00029 11.9268 4.99536C11.9317 4.99044 11.9268 4.99532 11.9268 4.99532L11.9269 4.99536C11.9317 5.00029 11.9293 4.9979 11.9268 4.9954Z" fill="white"></path><path d="M6.7442 11.7715H11.1622V10.8955C11.1622 10.4748 11.5032 10.1338 11.9239 10.1338C12.3446 10.1338 12.6856 10.4748 12.6856 10.8955V11.7715H17.2178C18.6458 11.7715 19.8076 12.9333 19.8076 14.3613C19.8076 14.782 19.4666 15.123 19.0459 15.123C18.6252 15.123 18.2842 14.782 18.2842 14.3613C18.2842 13.7733 17.8058 13.2949 17.2178 13.2949H12.6856V14.209C12.6856 14.6297 12.3446 14.9707 11.9239 14.9707C11.5032 14.9707 11.1622 14.6297 11.1622 14.209V13.2949H6.7442C6.1562 13.2949 5.6778 13.7733 5.6778 14.3613C5.6778 14.782 5.33678 15.123 4.91609 15.123C4.49539 15.123 4.15437 14.782 4.15437 14.3613C4.15437 12.9333 5.31618 11.7715 6.7442 11.7715Z" fill="white"></path></svg> 
                            Dành cho cộng tác viên
                        </a>
                    
                            <ul class="dropdown-menu">
                                <li>
                                    <a target="_blank" href="#">Chính sách cộng tác viên
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="#">Hướng dẫn đăng ký
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="#">Kiến thức
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="#">Hỏi đáp
                                    </a>
                                </li>
                            </ul>
                        </div>
                  
                    <!-- end DÀNH CHO NHÀ TUYỂN DỤNG -->


                </div>

                <div class="col-lg-6 col-md-12 col-12">
                bb
                </div>

            </div>
        </div>
    </div>
</div>
<!-- end second menu -->





<?php /*?>@if(!Auth::user() && !Auth::guard('company')->user())
	<div class="">my dive 2</div>
@endif<?php */?>